import { useState } from "react";
import { TaskForm, TaskList } from "./components";
import { Task } from "./types/Task";
import FilterButtons from "./components/FilterButtons";
import { DragDropContext, DropResult } from "react-beautiful-dnd";

function App() {
  const [tasks, setTasks] = useState<Task[]>([]);
  const [filterCompleted, setFilterCompleted] = useState<boolean | null>(null);
  const handleAddTask = (task: Task) => {
    setTasks([...tasks, task]);
  };

  const handleTaskComplete = (taskId: number) => {
    setTasks((prevTasks) =>
      prevTasks.map((task) =>
        task.id === taskId ? { ...task, completed: !task.completed } : task
      )
    );
  };

  const handleFilterChange = (completed: boolean | null) => {
    setFilterCompleted(completed);
  };

  const handleTaskMove = (result: DropResult) => {
    if (!result.destination) return;

    const reorderedTasks = Array.from(tasks);
    const [movedTask] = reorderedTasks.splice(result.source.index, 1);
    reorderedTasks.splice(result.destination.index, 0, movedTask);

    setTasks(reorderedTasks);
  };

  const filteredTasks =
    filterCompleted === null
      ? tasks
      : tasks.filter((task) => task.completed === filterCompleted);
  return (
    <DragDropContext onDragEnd={handleTaskMove}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          minHeight: "100vh",
          minWidth: "100vw",
        }}
      >
        <div
          style={{ padding: "16px", flex: 1, maxWidth: "600px", width: "100%" }}
        >
          <h1>Task Management App</h1>
          <TaskForm onAddTask={handleAddTask} />
          <FilterButtons
            filterCompleted={filterCompleted}
            handleFilterChange={handleFilterChange}
          />
          <TaskList tasks={filteredTasks} onTaskComplete={handleTaskComplete} />
        </div>
      </div>
    </DragDropContext>
  );
}

export default App;
