import React from "react";
import { Task } from "../types/Task";
import TaskListItem from "./TaskListItem";
import { Droppable } from "react-beautiful-dnd";
interface TaskListProps {
  tasks: Task[];
  onTaskComplete: (taskId: number) => void;
}

const TaskList: React.FC<TaskListProps> = ({ tasks, onTaskComplete }) => {
  return (
    <Droppable droppableId="task-list">
      {(provided) => (
        <div ref={provided.innerRef} {...provided.droppableProps}>
          {tasks.map((task, index) => (
            <TaskListItem
              key={task.id}
              task={task}
              index={index}
              onTaskComplete={onTaskComplete}
            />
          ))}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  );
};

export default TaskList;
