import React from "react";
import { Task } from "../types/Task";
import { Card, CardContent, Checkbox, Grid, Typography } from "@mui/material";
import { Draggable } from "react-beautiful-dnd";
interface TaskListItemProps {
  task: Task;
  index: number;
  onTaskComplete: (taskId: number) => void;
}

const TaskListItem: React.FC<TaskListItemProps> = ({
  task,
  index,
  onTaskComplete,
}) => {
  const handleCheckboxChange = () => {
    onTaskComplete(task.id);
  };

  return (
    <Draggable draggableId={`task-${task.id}`} index={index}>
      {(provided) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <Card
            sx={{
              margin: "8px",
              backgroundColor: task.completed ? "#d3ffd3" : "white",
            }}
          >
            <CardContent>
              <Grid container alignItems="center" spacing={2}>
                <Grid item>
                  <Checkbox
                    checked={task.completed}
                    onChange={handleCheckboxChange}
                  />
                </Grid>
                <Grid item>
                  <Typography
                    variant="h6"
                    sx={{
                      textDecoration: task.completed ? "line-through" : "none",
                    }}
                  >
                    {task.title}
                  </Typography>
                  <Typography variant="body1">{task.description}</Typography>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </div>
      )}
    </Draggable>
  );
};

export default TaskListItem;
