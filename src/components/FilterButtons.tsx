import React from "react";
import { Button } from "@mui/material";

interface IProps {
  filterCompleted: boolean | null;
  handleFilterChange: (value: boolean | null) => void;
}

export default function FilterButtons({
  filterCompleted,
  handleFilterChange,
}: IProps) {
  return (
    <div style={{ margin: "16px 0", display: "flex", gap: "8px" }}>
      <Button
        variant={filterCompleted === null ? "contained" : "outlined"}
        onClick={() => handleFilterChange(null)}
        color="primary"
        sx={{ flex: 1 }}
      >
        All
      </Button>
      <Button
        variant={filterCompleted === true ? "contained" : "outlined"}
        onClick={() => handleFilterChange(true)}
        color="primary"
        sx={{ flex: 1 }}
      >
        Completed
      </Button>
      <Button
        variant={filterCompleted === false ? "contained" : "outlined"}
        onClick={() => handleFilterChange(false)}
        color="primary"
        sx={{ flex: 1 }}
      >
        Not Completed
      </Button>
    </div>
  );
}
